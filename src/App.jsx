import { useState } from 'react'
import './App.css'
import Navbar from './components/Navbar'
import CartContainer from './components/CartContainer'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { calculateTotals, getCartItems } from './features/cart/cartSlice'
import Modal from './components/Modal'
function App() {

  const { cartItems, isLoading } = useSelector(store => store.cart)
  const { isOpen } = useSelector(store => store.modal)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(calculateTotals())
  }, [cartItems])


  useEffect(() => {
    dispatch(getCartItems())
  }, [])

  if (isLoading) {
    return <div className='loading'>Loading...</div>
  }
  return (
    <div className="App">
      {isOpen && <Modal />}
      <Navbar />
      <CartContainer />
    </div>
  )
}

export default App
